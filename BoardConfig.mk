USE_CAMERA_STUB := true

# inherit from the proprietary version
-include vendor/onda/k12_MA975M8/BoardConfigVendor.mk

TARGET_ARCH := arm
TARGET_NO_BOOTLOADER := true
BOARD_MKBOOTIMG_ARGS := --second device/onda/k12_MA975M8/kernel/meson8_onda_v975m_V1_staneV1.dtb
TARGET_BOARD_PLATFORM := meson8
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a
TARGET_CPU_VARIANT := generic
ARCH_ARM_HAVE_TLS_REGISTER := true
TARGET_NO_RADIOIMAGE := true

TARGET_RECOVERY_INITRC := device/onda/k12_MA975M8/recovery/init.rc

TARGET_BOOTLOADER_BOARD_NAME := k12_MA975M8

BOARD_KERNEL_CMDLINE := 
BOARD_KERNEL_BASE := 0x10008000
BOARD_KERNEL_PAGESIZE := 2048

# fix this up by examining /proc/mtd on a running device
# fix this up by examining /proc/block on a running device
# Boot image size is 33554432 bytes
# You can double check it with fdisk -l /dev/block/boot
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x2000000
# Recovery image size is 33554432 bytes
# You can double check it with fdisk -l /dev/block/recovery
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x2000000
# System image size is 1073741824
# You can double check it with fdisk -l /dev/block/system
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 0x40000000
# user data image size is 29444014080 bytes
# You can double check it with fdisk -l /dev/block/data
BOARD_USERDATAIMAGE_PARTITION_SIZE := 0x6DB000000
BOARD_FLASH_BLOCK_SIZE := 131072

TARGET_PREBUILT_KERNEL := device/onda/k12_MA975M8/kernel/boot.img-kernel.staneV1

BOARD_HAS_NO_SELECT_BUTTON := true
# Use this flag if the board has a ext4 partition larger than 2gb
BOARD_HAS_LARGE_FILESYSTEM := true

#twrp
DEVICE_RESOLUTION := 2048x1536
TARGET_RECOVERY_PIXEL_FORMAT := "RGB_565"
# TARGET_RECOVERY_PIXEL_FORMAT := "RGBA_8888"
RECOVERY_GRAPHICS_USE_LINELENGTH := true
TW_INTERNAL_STORAGE_PATH := "/mnt/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT := "TWRP"
TW_EXTERNAL_STORAGE_PATH := "/sdcard"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "/ext-sd"
TW_DEFAULT_EXTERNAL_STORAGE := true
TW_CUSTOM_POWER_BUTTON := 116
TW_FLASH_FROM_STORAGE := true
# enable touch logging
# TWRP_EVENT_LOGGING := true

# NAND Flash support module
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/recovery/boot/aml_nftl_dev.ko:recovery/root/boot/aml_nftl_dev.ko
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/recovery/etc/fw_env.config:recovery/root/etc/fw_env.config

# copy version 1 fstab for compatibility and use by auto-copy of twrp
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/twrp.fstab.staneV1:recovery/root/etc/twrp.fstab

# Touch related files
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/etc/touch/ft5x06.fw:recovery/root/etc/touch/ft5x06.fw
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/etc/touch/goodix.bin:recovery/root/etc/touch/goodix.bin
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/etc/touch/goodix.cfg:recovery/root/etc/touch/goodix.cfg
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/Vendor_222a_Product_0001.idc:recovery/root/system/usr/idc/Vendor_222a_Product_0001.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/Vendor_dead_Product_beef.idc:recovery/root/system/usr/idc/Vendor_dead_Product_beef.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/ft5x06.idc:recovery/root/system/usr/idc/ft5x06.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/mg-capacitive.idc:recovery/root/system/usr/idc/mg-capacitive.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/pixcir168.idc:recovery/root/system/usr/idc/pixcir168.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/qwerty.idc:recovery/root/system/usr/idc/qwerty.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/qwerty2.idc:recovery/root/system/usr/idc/qwerty2.idc
PRODUCT_COPY_FILES += device/onda/k12_MA975M8/touch/system/usr/idc/ssd253x-ts.idc:recovery/root/system/usr/idc/ssd253x-ts.idc

